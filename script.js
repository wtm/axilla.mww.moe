(async function () {
  let slides = [
    {
      main: "/slides/1.html",
      assets: []
    },
    {
      main: "/slides/2.html",
      assets: []
    },
    {
      main: "/slides/3.html",
      assets: [
        "/slides/img/3-1.png",
        "/slides/img/3-2.jpg"
      ]
    },
    {
      main: "/slides/4.html",
      assets: []
    },
    {
      main: "/slides/5.html",
      assets: [
        "/slides/img/5.png"
      ]
    },
    {
      main: "/slides/6.html",
      assets: [
        "/slides/svg/6.svg"
      ]
    },
    {
      main: "/slides/7.html",
      assets: [
        "/slides/svg/7.svg"
      ]
    },
    {
      main: "/slides/8.html",
      assets: [
        "/slides/svg/8.svg"
      ]
    },
    {
      main: "/slides/9.html",
      assets: [
        "/slides/img/9.jpg"
      ]
    },
    {
      main: "/slides/10.html",
      assets: [
        "/slides/svg/10.svg"
      ]
    },
    {
      main: "/slides/11.html",
      assets: [
        "/slides/img/11.png"
      ]
    },
    {
      main: "/slides/12.html",
      assets: []
    },
    {
      main: "/slides/13.html",
      assets: [
        "/svg/down.svg"
      ]
    },
    {
      main: "/slides/14.html",
      assets: []
    },
    {
      main: "/slides/15.html",
      assets: []
    },
    {
      main: "/slides/16.html",
      assets: [
        "/slides/svg/16.svg"
      ]
    },
    {
      main: "/slides/17.html",
      assets: []
    },
    {
      main: "/slides/18.html",
      assets: []
    },
  ];

  let pres_div = document.querySelector(".presentation");
  let progress_bar = pres_div.querySelector(".loading .fill");
  try {
    let nb_slides = slides.length;
    let nb_assets = slides.length + slides.reduce((acc, slide) => acc + slide.assets.length, 0);
    let loaded_assets = new Set();
    function update_progress() {
      let prog = (loaded_assets.size / nb_assets * 100).toFixed("1");
      progress_bar.style.width = prog + "%";
    }
    function load_asset(url) {
      url = url + "?cache-v=3";
      console.log(`fetch ${url}`)
      return fetch(url, {method: "GET", mode: "no-cors"}).then(res => {
        if (!res.ok) {
          return Promise.reject(new Error(`Failed to load asset ${url}: ${res.status}`));
        }
        loaded_assets.add(url);
        update_progress();
        return Promise.resolve(res);
      });
    }
    function load_text(url) {
      return load_asset(url).then(res => res.text());
    }
    function shake() {
      pres_div.classList.remove("shake");
      setTimeout(() => {
        pres_div.classList.add("shake");
      }, 0);
    }

    let labels = {};

    for (let i = 0; i < nb_slides; i ++) {
      let slide = document.createElement("div");
      slide.classList.add("slide");
      slide.classList.add("slide-" + (i + 1).toString());
      slide.style.display = "none";

      let s = slides[i];

      let slide_content = await load_text(s.main);
      slide.innerHTML = slide_content;
      pres_div.appendChild(slide);
      s.container = slide;
      for (let sp of slide.querySelectorAll(".spoiler")) {
        sp.addEventListener("click", () => {
          sp.classList.toggle("revealed");
        });
      }

      for (let a of s.assets) {
        await load_asset(a);
      }
      for (let inc of slide.querySelectorAll(".include")) {
        let src = inc.getAttribute("data-src");
        let content = await load_text(src);
        inc.innerHTML = content;
      }
      for (let shb of slide.querySelectorAll(".svg-hover-box")) {
        let svg_elem = slide.querySelector(shb.getAttribute("data-target"));
        if (!svg_elem) {
          throw new Error(`${shb.getAttribute("data-target")} not found.`);
        }
        svg_elem.style.fill = "#ffff00";
        svg_elem.style.fillOpacity = "0.1";
        svg_elem.style.stroke = "#ffd700";
        svg_elem.style.strokeOpacity = "0.7";
        svg_elem.style.strokeWidth = "2";
        svg_elem.style.cursor = "pointer";
        let locked = false;
        function update(evt) {
          if (locked) {
            shb.style.pointerEvents = "";
            let box = svg_elem.getBoundingClientRect();
            shb.style.left = box.left + "px";
            shb.style.right = "";
            shb.style.top = (box.top + box.height) + "px";
          } else {
            shb.style.pointerEvents = "none";
            if (evt.clientX + 400 < window.innerWidth) {
              shb.style.right = "";
              shb.style.left = (evt.clientX + 10) + "px";
            } else {
              shb.style.left = "";
              shb.style.right = (window.innerWidth - evt.clientX + 10) + "px";
            }
            shb.style.top = (evt.clientY + 10) + "px";
          }
        }
        function unlock(evt) {
          if (!shb.contains(evt.target) && !svg_elem.contains(evt.target)) {
            locked = false;
            shb.style.display = "none";
            document.removeEventListener("mousedown", unlock);
            svg_elem.style.fillOpacity = "0.1";
          }
        }
        svg_elem.addEventListener("mouseenter", evt => {
          if (locked) {
            return;
          }
          svg_elem.style.fillOpacity = "0.2";
          shb.style.display = "block";
          document.addEventListener("mousemove", update);
        });
        svg_elem.addEventListener("mousedown", evt => {
          locked = true;
          svg_elem.style.strokeOpacity = "1";
          svg_elem.style.fillOpacity = "0";
          update(evt);
          setTimeout(() => {
            document.addEventListener("mousedown", unlock);
          }, 10);
        })
        svg_elem.addEventListener("mouseleave", evt => {
          if (locked) {
            return;
          }
          svg_elem.style.fillOpacity = "0.1";
          shb.style.display = "none";
          document.removeEventListener("mousemove", update);
        });
      }
      for (let label of slide.querySelectorAll("[data-label]")) {
        let name = label.getAttribute("data-label");
        labels[name] = {
          elem: label,
          slide: i
        };
      }
      for (let ref of slide.querySelectorAll("[data-ref]")) {
        let target = ref.getAttribute("data-ref");
        // We deliberately postpond the getting of the label to be inside the event listener.
        ref.addEventListener("click", evt => {
          let label = labels[target];
          if (!label) {
            alert(`Invalid label ${target}`);
          }
          let { elem, slide } = label;
          show_slide(slide);
          elem.style.transition = "none";
          elem.style.backgroundColor = "yellow";
          setTimeout(() => {
            elem.style.transition = "background 1000ms";
            elem.style.backgroundColor = "";
          }, 0);
        });
      }

      for (let sel of slide.querySelectorAll(".selections")) {
        // https://stackoverflow.com/a/2450976/4013790
        function shuffle(children) {
          let currentIndex = children.length, randomIndex;

          // While there remain elements to shuffle.
          while (currentIndex != 0) {

            // Pick a remaining element.
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;

            // And swap it with the current element.
            let c = children[currentIndex];
            let r = children[randomIndex];
            let orig_sibling = c.nextSibling;
            c.parentNode.insertBefore(c, r);
            if (orig_sibling) {
              c.parentNode.insertBefore(r, orig_sibling);
            } else {
              c.parentNode.appendChild(r);
            }
          }
        }

        shuffle(sel.children);
      }

      for (let draggable of slide.querySelectorAll(".draggable")) {
        let origX, origY;
        let targets;
        let currX, currY;

        function handle_down(evt) {
          if (draggable.classList.contains("disabled")) {
            return;
          }
          evt.preventDefault();
          let bbox = draggable.getBoundingClientRect();
          origX = bbox.left + bbox.width / 2 + window.scrollX;
          origY = bbox.top + bbox.height / 2 + window.scrollY;

          let targetSelector = draggable.getAttribute("data-targets");
          targets = Array.from(slide.querySelectorAll(targetSelector));

          document.addEventListener("mousemove", handle_move);
          document.addEventListener("touchmove", handle_move);
          document.addEventListener("mouseup", handle_up);
          document.addEventListener("touchend", handle_up);
          document.addEventListener("touchcancel", handle_up);

          draggable.style.willChange = "transform";
        }

        function handle_move(evt) {
          evt.preventDefault();
          let pageX, pageY;
          if (evt.changedTouches && evt.changedTouches.length == 1) {
            pageX = evt.changedTouches[0].pageX;
            pageY = evt.changedTouches[0].pageY;
          } else {
            pageX = evt.pageX;
            pageY = evt.pageY;
          }
          currX = pageX;
          currY = pageY;

          draggable.style.transform = `translate(${pageX - origX}px, ${pageY - origY}px)`;
        }

        function handle_up(evt) {
          evt.preventDefault();
          document.removeEventListener("mousemove", handle_move);
          document.removeEventListener("touchmove", handle_move);
          document.removeEventListener("mouseup", handle_up);
          document.removeEventListener("touchend", handle_up);
          document.removeEventListener("touchcancel", handle_up);

          draggable.style.willChange = "";

          for (let target of targets) {
            let bbox = target.getBoundingClientRect();
            let targetCenterX = bbox.left + bbox.width / 2;
            let targetCenterY = bbox.top + bbox.height / 2;
            if (currX >= bbox.left && currX <= bbox.right && currY >= bbox.top && currY <= bbox.bottom) {
              let correct_targets = Array.from(slide.querySelectorAll(draggable.getAttribute("data-correct")));
              if (correct_targets.includes(target)) {
                draggable.style.transform = `translate(${targetCenterX - origX}px, ${targetCenterY - origY}px)`;
                draggable.classList.add("disabled");
              } else {
                shake();
                draggable.style.transform = "";
              }
              return;
            }
          }
          draggable.style.transform = "";
        }

        draggable.addEventListener("mousedown", handle_down);
        draggable.addEventListener("touchstart", handle_down);
      }

      for (let reveal_ans_btn of slide.querySelectorAll(".reveal-answers")) {
        reveal_ans_btn.addEventListener("click", evt => {
          let should_show = true;

          for (let ipt of slide.querySelectorAll("input.tofill")) {
            if (!ipt.value) {
              should_show = false;
              break;
            }
          }

          if (should_show) {
            for (let ans of slide.querySelectorAll(".answer")) {
              ans.style.visibility = "visible";
              ans.classList.add("show");
            }
          } else {
            shake();
          }
        });
      }
    }

    pres_div.querySelector(".loading").style.display = "none";
  } catch (e) {
    console.error(e);
    if (parseFloat(progress_bar.style.width) < 10) {
      progress_bar.style.width = "10%";
    }
    progress_bar.style.backgroundColor = "red";
    let text = document.createElement("div");
    text.innerText = e.message;
    text.classList.add("error");
    pres_div.appendChild(text);
    return;
  }

  let left = document.querySelector(".controls .left");
  let right = document.querySelector(".controls .right");
  let current_slide = 0;

  function show_slide(i) {
    if (i == current_slide) {
      for (let j = 0; j < slides.length; j ++) {
        if (j != i) {
          slides[j].container.style.display = "none";
        } else {
          slides[i].container.style.display = "";
          slides[i].container.style.transition = "none";
          slides[i].container.style.transform = "none";
        }
      }
    } else {
      for (let j = 0; j < slides.length; j ++) {
        if (j != i && j != current_slide) {
          slides[j].container.style.display = "none";
        } else {
          slides[j].container.style.display = "";
        }
        slides[j].container.style.transition = "none";
        slides[j].container.style.transform = "none";
      }
      slides[i].container.style.transform = (i < current_slide) ? "translateX(-100vw)" : "translateX(100vw)";
      slides[current_slide].container.style.transform = "translateX(0)";
      setTimeout((function (i, current_slide) {
        slides[i].container.style.transition = "transform 0.5s ease-in-out";
        slides[current_slide].container.style.transition = "transform 0.5s ease-in-out";
        slides[i].container.style.transform = "translateX(0)";
        slides[current_slide].container.style.transform = (i < current_slide) ? "translateX(100vw)" : "translateX(-100vw)";
      }).bind(null, i, current_slide), 0);
    }

    if (i == 0) {
      left.classList.add("disabled");
    } else {
      left.classList.remove("disabled");
    }

    if (i == slides.length - 1) {
      right.classList.add("disabled");
    } else {
      right.classList.remove("disabled");
    }
    current_slide = i;
  }

  left.addEventListener("click", evt => {
    if (!left.classList.contains("disabled")) {
      show_slide(current_slide - 1);
    }
  });
  right.addEventListener("click", evt => {
    if (!right.classList.contains("disabled")) {
      show_slide(current_slide + 1);
    }
  });

  function handle_resize() {
    let width = window.innerWidth;
    let height = window.innerHeight;
    // let font_size = 24;
    // if (width < 500 || height < 500) {
    //   font_size = 14;
    // } else if (width > 2000 && height > 1000) {
    //   font_size = 50;
    // } else if (width > 1300) {
    //   font_size = 36;
    // }
    // document.documentElement.style.fontSize = font_size + "px";
  }
  // handle_resize();
  // window.addEventListener("resize", handle_resize);

  show_slide(current_slide);
})();
